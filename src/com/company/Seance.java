package com.company;

public class Seance {
    protected Salle salle ;
    protected Creneau creneau ;
    protected int nbPlaceDispoTN ;
    protected int nbPlaceVendueTN ;

    public Seance(Creneau creneau, Salle salle){
        this.creneau = creneau ;
        this.salle = salle ;
        nbPlaceDispoTN = salle.getNbPlace() ;
        nbPlaceVendueTN = 0 ;
    }

    /**
     * @return le créneau de la séance
     */
    public Creneau getCreneau() { return creneau;}

    /**
     * @return le nombre de place disponible
     */
    public int getNbPlaceDispoTN() { return nbPlaceDispoTN; }

    /**
     * @return la salle de la séance
     */
    public Salle getSalle() {return salle; }

    /**
     * @return le nombre de place vendue
     */
    public int getNbPlaceVendueTN() { return nbPlaceVendueTN;}


    /**
     * Vend un nombre de place correspondant au nombre passé en paramètre
     * Test si le nombre passé en paramètre est positif et lève une exception sinon
     * @param nbre
     * @throws IllegalArgumentException Nombre de place à vendre invalide
     */
    public void vendrePlaceTN(int nbre){
        if(nbre<0)
            throw new IllegalArgumentException("Nombre de place à vendre invalide") ;
        this.nbPlaceDispoTN -= nbre ;
        this.nbPlaceVendueTN += nbre ;
    }

    public String toString(){ return("Séance du "+creneau.toString()+"\nNombre de places vendues : "+nbPlaceVendueTN) ;}

    public boolean equals(Object o){
        if(this == o) return true ;
        if(o == null || getClass() != o.getClass()) return false ;
        Seance that = (Seance) o ;
        return this.getSalle().equals(that.getSalle())
                && this.getCreneau().equals(that.getCreneau()) ;
    }
}
