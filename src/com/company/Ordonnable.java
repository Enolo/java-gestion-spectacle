package com.company;

public interface Ordonnable<T> extends Comparable<T> {
    default boolean estPlusGrand(T o){
        return this.compareTo(o)>0;
    }
    default boolean estPlusPetit(T o){
        return this.compareTo(o)<0;
    }
    default boolean estEgal(T o){
        return this.compareTo(o)==0;
    }

}
