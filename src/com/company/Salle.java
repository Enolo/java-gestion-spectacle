package com.company;

import java.util.*;

public class Salle {
    protected int numeroSalle;
    protected String nomSalle;
    protected int nbPlace;
    protected double tarifPlace;
    protected SortedMap<Integer, Set<Creneau>> lesCreneauxOccupes = new TreeMap<Integer, Set<Creneau>>();
    private static int nbInstance;

    public Salle(String nomSalle, int nbPlace, double tarifPlace) {
        this.numeroSalle = 10 + 10 * nbInstance;
        this.nomSalle = nomSalle;
        this.nbPlace = nbPlace;
        this.tarifPlace = tarifPlace;
        nbInstance++;
    }

    /**
     * @return le numéro de la salle
     */
    public int getNumeroSalle() { return numeroSalle; }

    /**
     * @return le nom de la salle
     */
    public String getNomSalle() { return nomSalle; }

    /**
     * @return le nombre de place
     */
    public int getNbPlace() { return nbPlace; }

    /**
     * @return le tarif d'une place
     */
    public double getTarifPlace() { return tarifPlace; }

    /**
     * Calcul le tarif réduit selon le prix d'un place standard passé en paramètre
     * @param tarifPlace
     * @return le tarif d'une place réduite
     */
    public double tarifReduit(double tarifPlace) { return (tarifPlace * 0.6); }

    /**
     * Test si le créneau passé en paramètre est disponible dans cette salle, renvoie faux sinon
     * @param c
     * @return
     */
    public boolean estDisponible(Creneau c) {
        Set<Map.Entry<Integer, Set<Creneau>>> lesEntrees = lesCreneauxOccupes.entrySet();
        Iterator<Map.Entry<Integer, Set<Creneau>>> it = lesEntrees.iterator();
        //Pour le premier créneau à ajouter
        if(!it.hasNext())
            return true ;

        while (it.hasNext()) {
            Map.Entry<Integer, Set<Creneau>> uneEntree = it.next();
            if (uneEntree.getKey() == c.getJourSemaine()) {
                //Récup créneauOccupés correspondants au jour
                Set<Creneau> creneauJour = this.lesCreneauxOccupes.get(c.getJourSemaine()) ;
                Iterator<Creneau> itCreneauJour = creneauJour.iterator() ;

                while(itCreneauJour.hasNext()){
                    //Compare avec le créneau d'avant
                    Creneau creneauCompare =  itCreneauJour.next() ;
                    if(creneauCompare.equals(c))
                        return false ;
                    if(!c.chevauchement(creneauCompare)) {
                        if(itCreneauJour.hasNext()){
                            //Compare avec le créneau d'après
                            Creneau creneauCompare2 = itCreneauJour.next() ;
                            if (!creneauCompare2.chevauchement(c)) {
                                return true;
                            }
                            else
                                return false ;
                    }
                        return true ; //Pas de créneau après : dernier de la liste
                    }
                }
            }
        }
        return false;
    }

    /**
     * Teste si un créneau est occupé dans le jour passé en paramètre, renvoie faux sinon
     * Teste si le jour passé en paramètre est correct et lève une exception sinon
     * @param jour
     * @return
     * @throws IllegalArgumentException Jour invalide
     */
    public boolean pasDeCreneauCeJour(int jour){
        if(jour < 1 || jour > 7)
            throw new IllegalArgumentException("Jour invalide") ;
        Set<Map.Entry<Integer, Set<Creneau>>> lesEntrees = lesCreneauxOccupes.entrySet();
        Iterator<Map.Entry<Integer, Set<Creneau>>> it = lesEntrees.iterator();
        while (it.hasNext()) {
            Map.Entry<Integer, Set<Creneau>> uneEntree = it.next();
            if (uneEntree.getKey() == jour)
                return false;
        }
        return true ;
    }

    /**
     * Ajoute le créneau passé en paramètre s'il est disponible dans cette salle, renvoie faux sinon
     * @param c
     * @return
     */
    public boolean ajouterCreneau(Creneau c){
        if(pasDeCreneauCeJour(c.getJourSemaine())){
            Set<Creneau> creneauJour = new TreeSet<Creneau>() ;
            creneauJour.add(c) ;
            lesCreneauxOccupes.put(c.getJourSemaine(), creneauJour) ;
            return true ;
        }
        else{
            if (estDisponible(c)){
                Set<Creneau> creneauJour = this.lesCreneauxOccupes.get(c.getJourSemaine()) ;
                creneauJour.add(c) ;
                return true ;
            }
            else
                return false ;
        }
    }

    public String toString(){
        return ("Salle n°"+numeroSalle+" / Nom Salle : "+nomSalle) ;
    }

    public boolean equals(Object o){
        if(this == o) return true ;
        if(o == null || getClass() != o.getClass()) return false ;
        Salle that = (Salle) o ;
        return this.getTarifPlace() == that.getTarifPlace()
                && this.getNbPlace() == that.getNbPlace()
                && this.getNomSalle().equals(that.getNomSalle())
                && this.getNumeroSalle() == that.getNumeroSalle() ;
    }
}
