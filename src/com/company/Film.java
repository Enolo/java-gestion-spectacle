package com.company;

public class Film extends Spectacle {
    private int idFilm ;
    private String nomRealisteur ;
    private int duree ;
    private static int nbInstance ;

    /**
     * Construit un film, test si la durée est positive et lève une exception sinon
     * @param titre
     * @param nomRealisteur
     * @param duree
     * @throws IllegalArgumentException Durée invalide
     */
    public Film(String titre, String nomRealisteur, int duree){
        super(titre);
        idFilm = 100 + nbInstance ;
        this.nomRealisteur = nomRealisteur ;
        if(duree < 0)
            throw new IllegalArgumentException("Durée invalide") ;
        this.duree = duree ;
        nbInstance ++ ;
    }
    /**
     * @return le nom du réalisateur
     */
    public String getNomRealisteur() { return nomRealisteur; }
    /**
     * @return l'ID du film
     */
    public int getIdFilm() { return idFilm;}
    /**
     * @return La durée du film
     */
    public int getDuree() {return duree;}


    /**
     * Ajoute une séance de film à la liste des séances et renvoie true
     * Renvoie false si la séance est déjà présente dans la liste
     * @param s
     * @return
     */
    public boolean ajouterSeanceFilm(SeanceFilm s){
        if (rechercheSeance(s.getCreneau().getJourSemaine(), s.getCreneau().getDebut()))
            return false ;
        else {
            listeSeance.add(s) ;
            return true;
        }
    }

    public String toString(){
        return ("Film n°"+idFilm+" / Nom du film : "+super.titre) ;
    }

    public boolean equals(Object o){
        if(this == o) return true ;
        if(o == null || getClass() != o.getClass()) return false ;
        Film that = (Film) o ;
        return this.getTitre().equals(that.getTitre())
                && this.getIdFilm() == that.getIdFilm()
                && this.getDuree() == that.getDuree()
                && this.getNomRealisteur().equals(that.getNomRealisteur());
    }
}
