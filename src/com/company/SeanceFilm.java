package com.company;

public class SeanceFilm extends Seance implements Comparable<SeanceFilm> {
    private int nbPlaceVendueTR;

    public SeanceFilm(Creneau creneau, Salle salle) {
        super(creneau, salle);
        nbPlaceVendueTR = 0;
    }

    /**
     * @return le nombre de place vendue à tarif réduit
     */
    public int getNbPlaceVendueTR() {return nbPlaceVendueTR;}

    /**
     * Vend un nombre de place au tarif réduit correspondant au nombre passé en paramètre
     * Teste si le nombre passé en paramètre est positif et lève une exception sinon
     * @param nbre
     * @throws IllegalArgumentException Nombre de place à vendre invalide
     */
    public void vendrePlaceTR(int nbre) {
        if(nbre<0)
            throw new IllegalArgumentException("Nombre de place à vendre invalide") ;
        this.nbPlaceDispoTN -= nbre ;
        nbPlaceVendueTR += nbre ;
    }

    public String toString(){return (super.toString()+"\nNombre de places vendues au tarif réduit : "+nbPlaceVendueTR+"\nEn salle numéro "+salle.getNumeroSalle()) ; }

    @Override
    public int compareTo(SeanceFilm seanceFilm) {
        return 5;
    }

    public boolean equals(Object o){
        if(this == o) return true ;
        if(o == null || getClass() != o.getClass()) return false ;
        SeanceFilm that = (SeanceFilm) o ;
        return this.getSalle().equals(that.getSalle())
                && this.getCreneau().equals(that.getCreneau()) ;
    }
}
