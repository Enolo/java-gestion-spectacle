package com.company;

public class SeanceTheatre extends Seance implements Comparable<SeanceTheatre> {
    private SalleTheatre salleTheatre ;
    private int nbFauteuilDispo;
    private int nbFauteuilVendu ;

    public SeanceTheatre(Creneau creneau, SalleTheatre salleTheatre){
        super(creneau,salleTheatre);
        this.salleTheatre = salleTheatre ;
        nbFauteuilDispo = salleTheatre.getNbFauteuil() ;
    }

    /**
     * @return la salle de la séance
     */
    public SalleTheatre getSalleTheatre() {return salleTheatre; }

    /**
     * @return le nombre de fauteuil disponible
     */
    public int getNbFauteuilDispo() { return nbFauteuilDispo; }

    /**
     * @return le nombre de fauteuil vendu
     */
    public int getNbFauteuilVendu() { return nbFauteuilVendu; }


    /**
     * Vend un nombre de fauteuil correspondant au nombre passé en paramètre
     * Teste si le nombre passé en paramètre est positif et lève une exception sinon
     * @param nbre
     * @throws IllegalArgumentException Nombre de fauteuil à vendre invalide
     */
    public void vendrePlaceFauteuil(int nbre){
        if(nbre<0)
            throw new IllegalArgumentException("Nombre de fauteuil à vendre invalide") ;
        this.nbFauteuilDispo -= nbre ;
        this.nbFauteuilVendu += nbre ;
    }

    public String toString(){ return (super.toString()+"\nNombre de places vendues au tarif fauteuil : "+nbFauteuilVendu+"\nEn salle numéro "+salleTheatre.getNumeroSalle()) ; }

    @Override
    public int compareTo(SeanceTheatre seanceTheatre) { return 5; }
}
