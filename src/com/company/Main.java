package com.company;

import java.util.Scanner;

public class Main {
    private static Scanner scanner = new Scanner(System.in) ;
    public static void main(String[]args){
        Scanner scan = new Scanner(System.in) ;
        GestionProgrammationSemaine programme = new GestionProgrammationSemaine() ;
        
        int choix = 10;
        do{
            try{
                choix = menu() ;
                switch (choix) {
                    case 0: //Réinitialiser la programmation
                        programme.reinitialiserProgrammation();
                        System.out.println("Programme réinitialisé");
                        break;
                    case 1: //Ajouter un film
                        System.out.println("Ajouter un film :\nTitre : ");
                        String titreF = scan.nextLine();
                        System.out.println("Réalisateur : ");
                        String rea = scan.nextLine();
                        System.out.println("Durée : ");
                        int duree = scan.nextInt();
                        programme.ajouterFilm(titreF, rea, duree);
                        scan.nextLine() ;//Equivalent du vidage du cache
                        System.out.println("Film ajouté.");
                        break;
                    case 2: //Ajouter pièce de théâtre
                        System.out.println("Ajouter une pièce\nTitre : ");
                        String titreP = scan.nextLine();
                        System.out.println("Metteur en scène : ");
                        String metteur = scan.nextLine();
                        System.out.println("Nombre d'entractes : ");
                        int entracte = scan.nextInt();
                        programme.ajouterPiece(titreP, metteur, entracte);
                        scan.nextLine() ;
                        System.out.println("Pièce ajoutée.");
                        break;
                    case 3: //Ajouter un nom d'interprète à un spectacle
                        if(programme.lesPieces() == "" && programme.lesFilms() == ""){
                            System.out.println("Aucun spectacle");
                            break ;
                        }
                        System.out.println("Ajouter un interprète");
                        System.out.println(programme.lesFilms());
                        System.out.println(programme.lesPieces());
                        System.out.println("Choisissez un film ou une pièce : ");
                        int idChoix = scan.nextInt();
                        if (!programme.existeFilm(idChoix) && !programme.existePiece(idChoix)) {
                            System.out.println("Numéro de film ou pièce inexistant");
                            break;
                        }
                        else {
                            System.out.println("Saisissez le nom de l'interprète : ");
                            String interprete = scan.nextLine();
                            programme.ajouterInterprete(idChoix, interprete);
                        }
                        scan.nextLine() ;
                        System.out.println("Interprète ajouté.");
                        break;
                    case 4: //Ajouter une séance pour un film
                        if (programme.lesFilms() == "") {
                            System.out.println("Aucun film");
                            break;
                        }
                        System.out.println("Ajouter une séance pour un film");
                        System.out.println(programme.lesFilms());
                        System.out.println("Choisissez un film : ");
                        int choixFilm = scan.nextInt();
                        if (!programme.existeFilm(choixFilm)) {
                            System.out.println("Numéro de film inexistant");
                            scan.nextLine() ;
                            break;
                        }
                        System.out.println("Jour : ");
                        int day = scan.nextInt();
                        System.out.println("Heures : ");
                        int heures = scan.nextInt();
                        System.out.println("Minutes : ");
                        int minutes = scan.nextInt();
                        System.out.println("Choix idSalle : ");
                        int idSalleFilm = scan.nextInt();
                        programme.ajouterSeanceFilm(choixFilm, day, new HoraireBis(heures, minutes), idSalleFilm);
                        scan.nextLine() ;
                        System.out.println("Séance ajoutée.");
                        break;
                    case 5: //Ajouter une séance pour une pièce de théâtre
                        if (programme.lesPieces() == "") {
                            System.out.println("Aucune pièce ");
                            break;
                        }
                        System.out.println("Ajouter une séance pour une pièce");
                        System.out.println(programme.lesPieces());
                        System.out.println("Choisissez une pièce : ");
                        int choixPiece = scan.nextInt();
                        if (!programme.existePiece(choixPiece)) {
                            System.out.println("Numéro de pièce inexistant");
                            scan.nextLine() ;
                            break;
                        }
                        System.out.println("Jour : ");
                        int jour = scan.nextInt();
                        if(programme.existeSeanceCeJour(choixPiece, jour)){
                            System.out.println("Il existe déjà une séance programmée à ce jour");
                            scan.nextLine() ;
                            break ;
                        }
                        System.out.println("Heures : ");
                        int heure = scan.nextInt();
                        System.out.println("Minutes : ");
                        int minute = scan.nextInt();
                        System.out.println("Choix idSalle : ");
                        int idSalleT = scan.nextInt();
                        programme.ajouterSeanceTheatre(choixPiece, jour, new HoraireBis(heure, minute), idSalleT);
                        scan.nextLine() ;
                        System.out.println("Séance ajoutée.");
                        break;
                    case 6: //Vendre des places pour un film
                        if (programme.lesFilms() == "") {
                            System.out.println("Aucun film");
                            break;
                        }
                        System.out.println("Vendre des places pour un film ");
                        System.out.println(programme.lesFilms());
                        System.out.println("Choisissez un film : ");
                        int idFilm = scan.nextInt();
                        if (!programme.existeFilm(idFilm)) {
                            System.out.println("Numéro de film inexistant");
                            scan.nextLine() ;
                            break;
                        }
                        if (programme.lesSeancesFilm(idFilm) == "") {
                            System.out.println("Aucune séance");
                            break;
                        }
                        System.out.println(programme.lesSeancesFilm(idFilm));
                        boolean check = false ;
                        int j = 0, hours=0, place=0, mins=0 ;
                        do{
                            try{
                                System.out.println("Jour : ");
                                j = scan.nextInt();
                                System.out.println("Heures : ");
                                hours = scan.nextInt();
                                System.out.println("Minutes : ");
                                mins = scan.nextInt();
                                place = programme.getNbPlacesDispo(idFilm, j, hours, mins);
                                check = true ;
                            }catch (IllegalArgumentException e){
                                System.out.println(e.getMessage());
                                scan.nextLine() ;
                                continue;
                            }
                        }while(!check) ;
                        System.out.println("Nombre de places disponibles : " + place);
                        System.out.println("Nombre de place à tarif normal à acheter : ");
                        int placeTN = scan.nextInt();
                        System.out.println("Nombre de place à tarif réduit à acheter : ");
                        int placeTR = scan.nextInt();
                        if(programme.getNbPlacesDispo(idFilm,j,hours,mins) < placeTN + placeTR)
                            throw new IllegalArgumentException("Pas assez de places disponibles") ;
                        programme.vendrePlaceFilmTN(idFilm, j, new HoraireBis(hours, mins), placeTN);
                        programme.vendrePlaceFilmTR(idFilm, j, new HoraireBis(hours, mins), placeTR);
                        scan.nextLine() ;
                        System.out.println("Vente effectuée.");
                        break;
                    case 7: //Vendre des places pour une pièce de théâtre
                        if (programme.lesPieces() == "") {
                            System.out.println("Aucune pièce");
                            break;
                        }
                        System.out.println("Vendre des places pour une pièce");
                        System.out.println(programme.lesPieces());
                        System.out.println("Choisissez une pièce : ");
                        int idPiece = scan.nextInt();
                        if (!programme.existePiece(idPiece)) {
                            System.out.println("Numéro de pièce inexistant");
                            scan.nextLine() ;
                            break;
                        }
                        if (programme.lesSeancesTheatre(idPiece) == "") {
                            System.out.println("Aucune séance");
                            break;
                        }
                        System.out.println(programme.lesSeancesTheatre(idPiece));
                        boolean test = false ;
                        int jourPiece = 0 ;
                        do{
                            try{
                                System.out.println("Jour : ");
                                jourPiece = scan.nextInt();
                                if(!programme.existeSeanceCeJour(idPiece,jourPiece))
                                    throw new IllegalArgumentException("Pas de séance à ce jour");
                                test = true ;
                            }catch (Exception e){
                                System.out.println(e.getMessage());
                                scan.nextLine();
                                continue;
                            }
                        }while(!test);
                        System.out.println("Nombre de places standards disponibles : " + programme.getNbPlacesDispo(idPiece, jourPiece));
                        System.out.println("Nombre de fauteuils disponibles : " + programme.getNbFauteuilsDispo(idPiece, jourPiece));
                        System.out.println("Nombre de place standard à acheter : ");
                        int placeP = scan.nextInt();
                        System.out.println("Nombre de fauteuil à acheter : ");
                        int placeF = scan.nextInt();
                        programme.vendrePlacePieceTN(idPiece, jourPiece, placeP);
                        programme.vendrePlaceFauteuilPiece(idPiece, jourPiece, placeF);
                        scan.nextLine() ;
                        System.out.println("Vente effectuée.");
                        break;
                    case 8 : //Consulter le chiffre d'affaire et le taux de remplissage d'un spectacle
                        if(programme.lesPieces() == "" && programme.lesFilms() == ""){
                            System.out.println("Aucun spectacle");
                            break ;
                        }
                        System.out.println("Consulter le chiffre d'affaire et le taux de remplissage");
                        System.out.println("Films et pièces disponibles : ");
                        System.out.println(programme.lesFilms());
                        System.out.println(programme.lesPieces());
                        System.out.println("Choisissez un spectacle : ");
                        int numSpect = scan.nextInt() ;
                        System.out.println("Taux de remplissage : "+programme.getTauxRemplissage(numSpect)+"%");
                        System.out.println("Chiffre d'affaire : "+programme.chiffreAffaires(numSpect)+"€");
                        scan.nextLine() ;
                        break ;
                    case 9 : //Quitter
                        System.out.println("Terminé.");
                }
            }catch(Exception e){
                System.out.println(e.getMessage());
                scan.nextLine() ;
                continue ; //Relance la boucle
            }
        }while(choix < 9) ;
    }

    static int menu(){
        int choix = 10 ;
        System.out.println();
        System.out.println("Réinitialiser la programmation : 0");
        System.out.println("Ajouter un film : 1");
        System.out.println("Ajouter pièce de théâtre : 2");
        System.out.println("Ajouter un nom d'interprète à un spectacle : 3");
        System.out.println("Ajouter une séance pour un film : 4");
        System.out.println("Ajouter une séance pour une pièce de théâtre : 5");
        System.out.println("Vendre des places pour un film : 6");
        System.out.println("Vendre des places pour une pièce de théâtre : 7");
        System.out.println("Consulter le chiffre d'affaire et le taux de remplissage d'un spectacle : 8");
        System.out.println("Quitter : 9");
        while((choix != 0) && (choix != 1) && (choix != 2) && (choix != 3) && (choix != 4) && (choix != 5)
                && (choix != 6) && (choix != 7) && (choix != 8) && (choix != 9)){
            choix = scanner.nextInt() ;
        }
        return choix ;
    }
}
