package com.company;

public class SalleTheatre extends Salle {
    private int nbFauteuil ;
    private double tarifFauteuil ;

    public SalleTheatre(String nomSalle, int nbPlace, double tarifPlace, int nbFauteuil, double tarifFauteuil){
        super(nomSalle,nbPlace,tarifPlace) ;
        this.nbFauteuil = nbFauteuil ;
        this.tarifFauteuil = tarifFauteuil ;
    }

    /**
     * @return le nombre de fauteuil
     */
    public int getNbFauteuil() { return nbFauteuil; }
    /**
     * @return le prix d'un fauteuil
     */
    public double getTarifFauteuil() { return tarifFauteuil; }

    public String toString(){ return (super.toString()+"\nNombre Fauteuil : "+nbFauteuil); }

    public boolean equals(Object o){
        if(this == o) return true ;
        if(o == null || getClass() != o.getClass()) return false ;
        SalleTheatre that = (SalleTheatre) o ;
        return super.equals(that)
                && this.getNbFauteuil() == that.getNbFauteuil()
                && this.getTarifFauteuil() == that.getTarifFauteuil() ;
    }
}
