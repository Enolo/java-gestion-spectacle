package com.company;

/**
 * @author utilisateur
 *
 */
public class HoraireBis implements Ordonnable<HoraireBis>{
	private int heures;
	private int minutes;

	/**
	 * Test si les heures sont bien comprises entre 0 et 23, lève une exception sinon
	 * Test si les minutes sont bien comprises entre 0 et 59, lève une exception sinon
	 * @throws IllegalArgumentException Heures invalides
	 * @throws IllegalArgumentException Minutes invalides
	 */
	public HoraireBis(int heures, int minutes) {
		if(heures > 23 || heures < 0)
			throw new IllegalArgumentException("Heures invalides") ;
		this.heures= heures;
		if(minutes > 59 || minutes < 0)
			throw new IllegalArgumentException("Minutes invalides") ;
		this.minutes = minutes;
	}
	/**
	 * @return the heures
	 */
	public int getHeures() {
		return heures;
	}
	/**
	 * @return the minutes
	 */
	public int getMinutes() {
		return minutes;
	}


	@Override
	public int compareTo(HoraireBis o) {
		if (this.heures>o.heures)
			return 1;
		else if (this.heures<o.heures)
			return -1;
		else {
			if (minutes>o.minutes)
				return 1;
			else if (minutes<o.minutes)
				return -1;
			else
				return 0;
		}
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		HoraireBis that = (HoraireBis) o;
		return getHeures() == that.getHeures() &&
				getMinutes() == that.getMinutes();
	}

	public String toString(){
		return(heures+"h"+minutes) ;
	}
}
