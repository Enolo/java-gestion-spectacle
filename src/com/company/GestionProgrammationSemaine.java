package com.company;

import java.util.*;

public class GestionProgrammationSemaine implements IProgrammationSemaine {
    private Map<Integer, Film> lesFilms = new HashMap<>() ;
    private Map<Integer, PieceTheatre> lesPieces = new HashMap<>() ;
    private Map<Integer, Salle> lesSalles = new HashMap<>() ;
    private Map<Integer, SalleTheatre> lesSallesTheatres = new HashMap<>() ;

    public GestionProgrammationSemaine(){
        Salle s1 = new Salle("S1", 100, 10) ;
        Salle s2 = new Salle("S2", 100, 10) ;
        Salle s3 = new Salle("S3", 100, 10) ;
        Salle s4 = new Salle("S4", 100, 10) ;
        lesSalles.put(s1.getNumeroSalle(),s1) ;
        lesSalles.put(s2.getNumeroSalle(),s2) ;
        lesSalles.put(s3.getNumeroSalle(),s3) ;
        lesSalles.put(s4.getNumeroSalle(),s4) ;

        SalleTheatre st1 = new SalleTheatre("ST1",50,20,25,30) ;
        SalleTheatre st2 = new SalleTheatre("ST2",50,20,25,30) ;
        SalleTheatre st3 = new SalleTheatre("ST3",50,20,25,30) ;
        SalleTheatre st4 = new SalleTheatre("ST4",50,20,25,30) ;
        lesSallesTheatres.put(st1.getNumeroSalle(), st1) ;
        lesSallesTheatres.put(st2.getNumeroSalle(), st2) ;
        lesSallesTheatres.put(st3.getNumeroSalle(), st3) ;
        lesSallesTheatres.put(st4.getNumeroSalle(), st4) ;
    }

    /**
     * Cherche la salle correspondant à l'ID passé en paramètre dans la liste des salles
     * Retourne la salle ou null si elle n'existe pas
     * @param idSalle
     * @return
     */
    public Salle getSalleById(int idSalle){
        for(Map.Entry<Integer, Salle> s : lesSalles.entrySet()){
            if(s.getKey() == idSalle)
                return s.getValue() ;
        }
        return null ;
    }

    /**
     * Cherche la salle de théâtre correspondant à l'ID passé en paramètre dans la liste des salles de théâtre
     * Retourne la salle ou null si elle n'existe pas
     * @param idSalle
     * @return
     */
    public SalleTheatre getSalleTheatreById(int idSalle){
        for(Map.Entry<Integer, SalleTheatre> s : lesSallesTheatres.entrySet()){
            if(s.getKey() == idSalle)
                return s.getValue() ;
        }
        return null ;
    }

    /**
     * Cherche la séance de film correspondant aux informations passées en paramaètre dans la liste des séances
     * Retourne la séance ou null si elle n'existe pas
     * @param idFilm
     * @param jour
     * @param heures
     * @param minutes
     * @return
     */
    public Seance getSeanceFilm(int idFilm, int jour, int heures, int minutes) {
        HoraireBis debut = new HoraireBis(heures, minutes);
        for (Map.Entry<Integer, Film> f : lesFilms.entrySet()) {
            if (f.getKey() == idFilm) {
                Film film = f.getValue();
                TreeSet<Seance> seance = film.getListeSeance();
                for (Seance s : seance) {
                    if (s.getCreneau().getJourSemaine() == jour && s.getCreneau().getDebut().equals(debut))
                        return s;
                }
            }
        }
        return null ;
    }

    /**
     * Cherche la séance de la pièce correspondant aux informations passées en paramaètre dans la liste des séances
     * Retourne la séance ou null si elle n'existe pas
     * @param idPiece
     * @param jour
     * @return
     */
    public Seance getSeanceTheatre(int idPiece, int jour){
        for(Map.Entry<Integer, PieceTheatre> p : lesPieces.entrySet()){
            if(p.getKey() == idPiece){
                PieceTheatre piece = p.getValue() ;
                TreeSet<Seance> seance = piece.getListeSeance() ;
                for(Seance s : seance){
                    if(s.getCreneau().getJourSemaine() == jour)
                        return s ;
                }
            }
        }
        return null ;
    }


    @Override
    public Film rechercherFilm(String titre, String realisateur) {
        for (Map.Entry<Integer, Film> f : lesFilms.entrySet()) {
            Film film = f.getValue();
            if(film.getTitre().equals(titre) && film.getNomRealisteur().equals(realisateur))
                return film ;
        }
        return null ;
    }

    @Override
    public void ajouterFilm(String titre, String realisateur, int duree) {
        if(rechercherFilm(titre,realisateur) == null){
            Film f = new Film(titre,realisateur,duree) ;
            lesFilms.put(f.getIdFilm(), f) ;
        }
        else
            throw new IllegalArgumentException("Le film existe déjà") ;
    }

    @Override
    public void ajouterInterprete(int numSpectacle, String interprete) {
        if(getSpectacle(numSpectacle) != null){
            getSpectacle(numSpectacle).getInterpretes().add(interprete) ;
        }
        else
            throw new IllegalArgumentException("Spectacle inexistant") ;

    }

    @Override
    public PieceTheatre rechercherPiece(String titre, String metteurEnScene) {
        for(Map.Entry<Integer, PieceTheatre> p : lesPieces.entrySet()){
            PieceTheatre piece = p.getValue() ;
            if(piece.getTitre().equals(titre) && piece.getNomMetteurEnScene().equals(metteurEnScene))
                return piece ;
        }
        return null;
    }

    @Override
    public void ajouterPiece(String titre, String metteurEnScene, int nbEntractes) {
        if(rechercherPiece(titre,metteurEnScene) == null){
            PieceTheatre p = new PieceTheatre(titre,metteurEnScene,nbEntractes) ;
            lesPieces.put(p.getIdPiece(), p) ;
        }
        else
            throw new IllegalArgumentException("La pièce existe déjà") ;
    }

    @Override
    public void ajouterSeanceFilm(int idFilm, int jour, HoraireBis debut, int idSalle) {
        if(existeFilm(idFilm)){
            if(existeSalleFilm(idSalle)){
                int duree = dureeFilm(idFilm) ;
                int dureeHeure = 0 ;
                while(duree >= 60){
                    dureeHeure++ ;
                    duree -= 60 ;
                }
                int dureeMin = duree ;
                int finHeure = debut.getHeures() + dureeHeure ;
                int finMin = debut.getMinutes() + dureeMin ;
                while(finMin >= 60){
                    finHeure ++ ;
                    finMin -= 60 ;
                }
                HoraireBis fin = new HoraireBis(finHeure, finMin) ;
                Creneau c = new Creneau(jour,debut,fin) ;
                if(salleStandardDisponible(jour,debut,duree,idSalle)){
                    Salle s = getSalleById(idSalle) ;
                    s.ajouterCreneau(c) ;
                    SeanceFilm sf = new SeanceFilm(c,s) ;
                    Film f = (Film) getSpectacle(idFilm) ;
                    f.ajouterSeanceFilm(sf) ;
                }
                else
                    throw new IllegalStateException("Créneau indisponible pour dans cette salle") ;
            }
            else
                throw new IllegalArgumentException("Salle inexistante") ;
        }
        else
            throw new IllegalArgumentException("Film inexistant") ;
    }

    @Override
    public boolean existeSeanceCeJour(int idPiece, int jour) {
        if(existePiece(idPiece)){
            for(Map.Entry<Integer, PieceTheatre> p : lesPieces.entrySet()){
                if(p.getKey() == idPiece){
                    PieceTheatre piece = p.getValue() ;
                    TreeSet<Seance> seance = piece.getListeSeance() ;
                    for(Seance s : seance){
                        if(s.getCreneau().getJourSemaine() == jour){
                            return true ;
                        }
                    }
                }
            }
        }
        else
            throw new IllegalArgumentException("Pièce inexistante") ;
        return false;
    }

    @Override
    public void ajouterSeanceTheatre(int idPiece, int jour, HoraireBis debut, int idSalle) {
        if(existePiece(idPiece)){
            if(existeSalleTheatre(idSalle)){
                int heureFin = debut.getHeures() + 3  ;
                if(heureFin >= 24)
                    heureFin = heureFin - 24 ;
                HoraireBis fin = new HoraireBis(heureFin,debut.getMinutes()) ;
                Creneau c = new Creneau(jour,debut,fin) ;
                if(getSalleTheatreById(idSalle).estDisponible(c)){
                    SalleTheatre salleT = getSalleTheatreById(idSalle) ;
                    salleT.ajouterCreneau(c) ;
                    SeanceTheatre st = new SeanceTheatre(c,salleT) ;
                    PieceTheatre piece = (PieceTheatre) getSpectacle(idPiece) ;
                    if(!existeSeanceCeJour(idPiece,jour))
                        piece.ajouterSeanceTheatre(st) ;
                }
                else
                    throw new IllegalStateException("Créneau indisponible pour dans cette salle") ;
            }
            else
                throw new IllegalArgumentException("Salle inexistante") ;
        }
        else
            throw new IllegalArgumentException("Pièce inexistante") ;
    }

    @Override
    public double chiffreAffaires(int numSpectacle) {
        if(getSpectacle(numSpectacle) != null)
            return getSpectacle(numSpectacle).getChiffreAffaire() ;
        else
            throw new IllegalArgumentException("Spectacle inexistant") ;
    }

    @Override
    public double getTauxRemplissage(int numSpectacle) {
        if(getSpectacle(numSpectacle) != null)
            return getSpectacle(numSpectacle).getTauxRempMoyen() ;

        else
            throw new IllegalArgumentException("Spectacle inexistant") ;
    }

    @Override
    public void vendrePlaceFilmTN(int idFilm, int jour, HoraireBis debut, int nbPlacesTN) {
        if(existeFilm(idFilm)){
            if(existeSeanceFilm(idFilm,jour,debut.getHeures(),debut.getMinutes())){
                SeanceFilm sF = (SeanceFilm) getSeanceFilm(idFilm,jour,debut.getHeures(),debut.getMinutes()) ;
                if(sF.getNbPlaceDispoTN() - nbPlacesTN > 0 ){
                    sF.vendrePlaceTN(nbPlacesTN) ;
                }
                else
                    throw new IllegalArgumentException("Pas assez de places disponibles") ;
            }
            else
                throw new IllegalArgumentException("Séance inexistante") ;
        }
        else
            throw new IllegalArgumentException ("Film inexistant") ;
    }

    @Override
    public void vendrePlaceFilmTR(int idFilm, int jour, HoraireBis debut, int nbPlacesTR) {
        if(existeFilm(idFilm)){
            if(existeSeanceFilm(idFilm,jour,debut.getHeures(),debut.getMinutes())){
                SeanceFilm sF = (SeanceFilm) getSeanceFilm(idFilm,jour,debut.getHeures(),debut.getMinutes()) ;
                if(sF.getNbPlaceDispoTN() - nbPlacesTR > 0 )
                    sF.vendrePlaceTR(nbPlacesTR) ;
                else
                    throw new IllegalArgumentException("Pas assez de places disponibles") ;
            }
            else
                throw new IllegalArgumentException("Séance inexistante") ;
        }
        else
            throw new IllegalArgumentException ("Film inexistant") ;
    }

    @Override
    public void vendrePlacePieceTN(int idPiece, int jour, int nbPlacesTN) {
        if(existePiece(idPiece)){
            if(existeSeanceCeJour(idPiece,jour)){
                SeanceTheatre sT = (SeanceTheatre) getSeanceTheatre(idPiece,jour) ;
                if(sT.getNbPlaceDispoTN() - nbPlacesTN > 0)
                    sT.vendrePlaceTN(nbPlacesTN);
                else
                    throw new IllegalArgumentException("Pas assez de fauteuils") ;
            }
            else
                throw new IllegalArgumentException("Séance inexistante") ;
        }
        else
            throw new IllegalArgumentException("Pièce inexistante") ;
    }

    @Override
    public void vendrePlaceFauteuilPiece(int idPiece, int jour, int nbFauteuils) {
        if(existePiece(idPiece)){
            if(existeSeanceCeJour(idPiece,jour)){
                SeanceTheatre sT = (SeanceTheatre) getSeanceTheatre(idPiece,jour) ;
                if(sT.getNbFauteuilDispo() - nbFauteuils > 0 )
                    sT.vendrePlaceFauteuil(nbFauteuils);
                else
                    throw new IllegalArgumentException("Pas assez de fauteuils") ;
            }
            else
                throw new IllegalArgumentException("Séance inexistante") ;
        }
        else
            throw new IllegalArgumentException("Pièce inexistante") ;
    }

    @Override
    public String lesFilms() {
        String str = "" ;
        for(Map.Entry<Integer, Film> f : lesFilms.entrySet()){
            Film film = f.getValue() ;
            str += film.toString()+"\n-------------------------------------------\n" ;
        }
        return str ;
    }

    @Override
    public String lesPieces() {
        String str = "";
        for(Map.Entry<Integer, PieceTheatre> p : lesPieces.entrySet()){
            PieceTheatre piece = p.getValue() ;
            str += piece.toString() +"\n-------------------------------------------\n" ;
        }
        return str ;
    }

    @Override
    public String lesSallesFilm() {
        String str = "" ;
        for(Map.Entry<Integer, Salle> salleF : lesSalles.entrySet()){
            Salle salle = salleF.getValue() ;
            str += salle.toString()+"\n-------------------------------------------\n" ;
        }
        return str;
    }

    @Override
    public String lesSallesTheatre() {
        String str = "" ;
        for(Map.Entry<Integer, SalleTheatre> salleT : lesSallesTheatres.entrySet()){
            SalleTheatre salle = salleT.getValue() ;
            str += salle.toString()+"\n-------------------------------------------\n" ;
        }
        return str;
    }

    @Override
    public String lesSeancesTheatre(int idPiece) {
        String str = "" ;
        if(getSpectacle(idPiece) != null){
            TreeSet<Seance> s = getSpectacle(idPiece).getListeSeance() ;
            int i = 1 ;
            while(i < 7) {
                Set<Seance> seanceJ = getSpectacle(idPiece).affichageSeanceJour(i);
                for(Seance seance : seanceJ)
                    str += seance.toString()+"\n--------------------------------\n" ;
                i++ ;
            }
            return str ;
        }
        else
            throw new IllegalArgumentException("Pièce inexistante") ;
    }

    @Override
    public String lesSeancesFilm(int idFilm) {
        String str = "" ;
        if(getSpectacle(idFilm) != null){
            TreeSet<Seance> s = getSpectacle(idFilm).getListeSeance() ;
            int i = 1 ;
            while(i< 7){
                Set<Seance> seanceJ = getSpectacle(idFilm).affichageSeanceJour(i);
                for(Seance seance : seanceJ)
                    str += seance.toString()+"\n---------------------------------------\n" ;
                i++ ;
            }
            return str ;
        }
        else
            throw new IllegalArgumentException("Film inexistant") ;
    }

    @Override
    public int getNbPlacesDispo(int numSpectacle, int jour, int heures, int minutes) {
        if (getSpectacle(numSpectacle) != null) {
            if (existeSeanceFilm(numSpectacle, jour, heures, minutes)) {
                Seance seanceF = getSeanceFilm(numSpectacle, jour, heures, minutes);
                return seanceF.getNbPlaceDispoTN();
            } else
                throw new IllegalArgumentException("Séance inexistante pour ce spectacle");
        }
        else
            throw new IllegalArgumentException("Spectacle inexistant") ;
    }

    @Override
    public boolean existeFilm(int idFilm) {
        for(Map.Entry<Integer, Film> f : lesFilms.entrySet()){
            if(f.getKey() == idFilm)
                return true ;
        }
        return false;
    }

    @Override
    public boolean existePiece(int idPiece) {
        for(Map.Entry<Integer, PieceTheatre> p : lesPieces.entrySet()){
            if(p.getKey() == idPiece)
                return true ;
        }
        return false;
    }

    @Override
    public boolean existeSeanceFilm(int idFilm, int jour, int heures, int minutes) {
        if(existeFilm(idFilm)){
            HoraireBis debut = new HoraireBis(heures,minutes) ;
            for(Map.Entry<Integer, Film> f : lesFilms.entrySet()){
                if(f.getKey() == idFilm){
                    Film film = f.getValue() ;
                    TreeSet<Seance> seance = film.getListeSeance() ;
                    for(Seance s : seance){
                        if(s.getCreneau().getJourSemaine() == jour && s.getCreneau().getDebut().equals(debut))
                            return true ;
                    }
                }
            }
        }
        else
            throw new IllegalArgumentException("Film inexistant") ;
        return false;
    }

    @Override
    public boolean existeSalleFilm(int idSalle) {
        for(Map.Entry<Integer, Salle> sf : lesSalles.entrySet()){
            if(sf.getKey() == idSalle)
                return true ;
        }
        return false;
    }

    @Override
    public boolean existeSalleTheatre(int idSalle) {
        for(Map.Entry<Integer, SalleTheatre> st : lesSallesTheatres.entrySet()){
            if(st.getKey() == idSalle)
                return true ;
        }
        return false;
    }

    @Override
    public int dureeFilm(int idFilm) {
        if(existeFilm(idFilm)){
            for(Map.Entry<Integer, Film> f : lesFilms.entrySet()){
                if(f.getKey() == idFilm){
                    return f.getValue().getDuree() ;
                }
            }
        }
        else
            throw new IllegalArgumentException("Film inexistant") ;
        return 0 ;
    }

    @Override
    public boolean salleStandardDisponible(int jour, HoraireBis debut, int duree, int idSalle) {
        if(existeSalleFilm(idSalle)){
            int finHeure = duree / 60 ;
            int finMin = duree - finHeure ;
            HoraireBis fin = new HoraireBis(finHeure, finMin) ;
            Creneau c = new Creneau(jour,debut,fin) ;
            if(getSalleById(idSalle).estDisponible(c)){
                return true ;
            }
            else
                return false ;
        }
        else
            throw new IllegalArgumentException("Salle inexistante") ;
    }

    @Override
    public void reinitialiserProgrammation() {
        lesFilms.clear();
        lesPieces.clear();
    }

    @Override
    public int getNbFauteuilsDispo(int idPiece, int jour) {
        if(getSpectacle(idPiece) != null){
            if(existeSeanceCeJour(idPiece,jour)){
                SeanceTheatre seanceP = (SeanceTheatre) getSeanceTheatre(idPiece,jour) ;
                return seanceP.getNbFauteuilDispo() ;
            }
            else
                throw new IllegalArgumentException("Aucune séance ce jour") ;
        }
        else
            throw new IllegalArgumentException("Pièce inexistante") ;
    }

    @Override
    public int getNbPlacesDispo(int idPiece, int jour) {
        if(getSpectacle(idPiece) != null){
            if(existeSeanceCeJour(idPiece,jour)){
                Seance seanceP = getSeanceTheatre(idPiece,jour);
                return seanceP.getNbPlaceDispoTN() ;
            }
            else
                throw new IllegalArgumentException("Aucune séance ce jour") ;
        }
        else
            throw new IllegalArgumentException("Pièce inexistante") ;
    }

    @Override
    public boolean estUnFilm(int numSpectacle) {
        if(numSpectacle >= 100 && numSpectacle <= 999)
            return true ;
        else
            return false ;
    }

    @Override
    public boolean estUnePiece(int numSpectacle) {
        if(numSpectacle >= 1000)
            return true ;
        else
            return false;
    }

    @Override
    public Spectacle getSpectacle(int numSpectacle) {
        for(Map.Entry<Integer, Film> f : lesFilms.entrySet()){
            if(f.getValue().getIdFilm() == numSpectacle)
                return f.getValue() ;
        }
        for(Map.Entry<Integer, PieceTheatre> p : lesPieces.entrySet()){
            if(p.getValue().getIdPiece() == numSpectacle){

                return  p.getValue() ;
            }
        }
        return null;
    }
}
