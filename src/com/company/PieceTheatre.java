package com.company;

import java.util.Set;

public class PieceTheatre extends Spectacle {
    private int idPiece ;
    private String nomMetteurEnScene ;
    private int nbEntractes ;
    private static int nbInstance ;

    /**
     * Construit une pièce, test si le nombre d'entracte est positif et lève une exception sinon
     * @param titre
     * @param nomMetteurEnScene
     * @param nbEntractes
     * @throws IllegalArgumentException Nombre d'entracte invalide
     */
    public PieceTheatre(String titre, String nomMetteurEnScene, int nbEntractes){
        super(titre) ;
        idPiece = 1000 + nbInstance ;
        this.nomMetteurEnScene = nomMetteurEnScene ;
        if(nbEntractes < 0)
            throw new IllegalArgumentException("Nombre d'entracte invalide") ;
        this.nbEntractes = nbEntractes ;
        nbInstance++ ;
    }

    /**
     * @return l'ID de la pièce
     */
    public int getIdPiece() { return idPiece; }

    /**
     * @return le nom du metteur en scène
     */
    public String getNomMetteurEnScene() { return nomMetteurEnScene; }

    /**
     * @return le nombre d'entractes
     */
    public int getNbEntractes() { return nbEntractes; }

    /**
     * Ajoute une séance de théâtre à la liste des séances et renvoie true
     * Renvoie false si la séance est déjà présente dans la liste
     * @param s
     * @return
     */
    public boolean ajouterSeanceTheatre(SeanceTheatre s){
        Set<Seance> seanceJour  = affichageSeanceJour(s.getCreneau().getJourSemaine()) ;
        if (seanceJour != null){
            for(Seance seance : seanceJour){
                if(seance.getClass().getSimpleName().equals(s.getClass().getSimpleName()))
                    return false ;
            }
        }
        listeSeance.add(s) ;
        return true;
    }

    public String toString(){ return ("Pièce n°"+idPiece+" / Nom de la pièce : "+super.titre) ; }

    public boolean equals(Object o ){
        if(this == o) return true ;
        if(o == null || getClass() != o.getClass()) return false ;
        PieceTheatre that = (PieceTheatre) o ;
        return this.getIdPiece() == that.getIdPiece()
                && this.getNomMetteurEnScene().equals(that.getNomMetteurEnScene())
                && this.getNbEntractes() == that.getNbEntractes()
                && this.getTitre().equals(that.getTitre());
    }
}
