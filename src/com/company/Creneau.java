package com.company;

public class Creneau implements Ordonnable<Creneau> {
    private int jourSemaine;
    private HoraireBis debut;
    private HoraireBis fin;

    /**
     * Construit un nouveau créneau
     * Teste si le jour de la semaine est bien entre 1 et 7 et lève une exception sinon
     * @param jourSemaine
     * @param debut
     * @param fin
     * @throws IllegalArgumentException Jour de la semaine invalide
     */
    public Creneau(int jourSemaine, HoraireBis debut, HoraireBis fin) {
        this.debut = new HoraireBis(debut.getHeures(), debut.getMinutes());
        this.fin = new HoraireBis(fin.getHeures(), fin.getMinutes());
        if (jourSemaine < 1 || jourSemaine > 7)
            throw new IllegalArgumentException("Jour de la semaine invalide");
        this.jourSemaine = jourSemaine;
    }

    /**
     * @return l'horaire de début
     */
    public HoraireBis getDebut() { return debut; }

    /**
     * @return l'horaire de fin
     */
    public HoraireBis getFin() { return fin; }

    /**
     * @return le jour de la semaine
     */
    public int getJourSemaine() { return jourSemaine; }

    /**
     * Test s'il y a un chevauchement entre this et le créneau passé en paramètre et renvoie true
     * Renvoie false s'ils ne se chevauchent pas
     * @param c
     * @return
     */
    public boolean chevauchement(Creneau c){
        if(this.getDebut().estPlusGrand(c.getDebut()) && this.getDebut().estPlusPetit(c.getFin()))
            return true ;
        else if(this.getFin().estPlusGrand(c.getDebut()) && this.getFin().estPlusPetit(c.getFin()))
            return true ;
        else
            return false ;
    }

    public String toString() { return (jourSemaine+" "+debut+" "+fin); }

    @Override
    public int compareTo(Creneau creneau) {
        return 5;
    }

    @Override
    public boolean equals(Object o){
        if(this == o) return true ;
        if (o == null || getClass() != o.getClass()) return false;
        Creneau that = (Creneau) o ;
        return this.getJourSemaine() == that.getJourSemaine()
                && this.getDebut().equals(that.getDebut())
                 && this.getFin().equals(that.getFin()) ;
    }

}
