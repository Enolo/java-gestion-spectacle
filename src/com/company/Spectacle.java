package com.company;

import java.util.*;

public class Spectacle {
    protected int numSpectacle ;
    protected String titre ;
    protected Set<String> interpretes = new HashSet<>() ;
    protected TreeSet<Seance> listeSeance = new TreeSet<>() ;

    public Spectacle(String titre){
        numSpectacle = 1   ;
        this.titre = titre ;
    }

    /**
     * @return le titre du spectacle
     */
    public String getTitre() {
        return titre;
    }

    /**
     * @return la liste des séances
     */
    public TreeSet<Seance> getListeSeance() {
        return listeSeance;
    }

    /**
     * @return la liste des interprètes
     */
    public Set<String> getInterpretes(){return interpretes ;}

    /**
     * Teste l'existence d'une séance correspondant au jour et à l'horaire de début si elle existe, renvoie faux sinon
     * Teste la validité du jour passé en paramètre
     * @param jour
     * @param debut
     * @throws IllegalArgumentException Jour invalide
     */
    public boolean rechercheSeance(int jour, HoraireBis debut){
        if(jour < 1 || jour > 7)
            throw new IllegalArgumentException("Jour invalide") ;
        for(Seance seance : listeSeance){
            if(seance.getCreneau().getJourSemaine() == jour && seance.getCreneau().getDebut().equals(debut))
                return true ;
        }
        return false ;
    }

    /**
     * Créer une collection des séances correspondant au jour passé en paramètre
     * Teste la validité du jour passé en paramètre
     * @param jour
     * @throws IllegalArgumentException Jour invalide
     */
    public Set<Seance> affichageSeanceJour(int jour){
        if(jour < 1 || jour > 7)
            throw new IllegalArgumentException("Jour invalide") ;
        Set<Seance> seanceJour = new TreeSet<Seance>() ;
        for(Seance seance : listeSeance){
            if (seance.getCreneau().getJourSemaine() == jour){
                seanceJour.add(seance) ;
            }
        }
        return seanceJour ;
        }

    /**
     * Calcul le temps de remplissage moyen d'un spectacle
     */
    public int getTauxRempMoyen(){
        double tauxMoyen = 0;

        for(Seance seance : listeSeance){
            if(seance instanceof SeanceFilm){
                SeanceFilm sF = (SeanceFilm) seance;
                tauxMoyen += Double.valueOf(sF.getNbPlaceVendueTN()) / sF.getSalle().getNbPlace();
            }
            else if(seance instanceof SeanceTheatre){
                SeanceTheatre sT = (SeanceTheatre) seance;
                tauxMoyen += Double.valueOf(sT.getSalleTheatre().getNbPlace() + sT.getSalleTheatre().getNbFauteuil())
                            / (sT.getNbPlaceVendueTN() + sT.getNbFauteuilVendu());
            }
        }
        return (int) (tauxMoyen / listeSeance.size() * 100);
        /*
         15(int) / 3 (int) => division entiere
         15(double) / 3(int) => division normal
         */
    }

    /**
     * Calcul le chiffre d'affaire total d'un spectacle
     */
    public double getChiffreAffaire(){
        double benefPlaceTN = 0 ;
        double benefFauteuil = 0 ;
        double benefPlaceTR = 0 ;
        for(Seance s : listeSeance){
            if(s instanceof SeanceTheatre){
                SeanceTheatre sT = (SeanceTheatre) s ;
                benefPlaceTN += sT.getNbPlaceVendueTN() * sT.getSalleTheatre().getTarifPlace() ;
                benefFauteuil += sT.getNbFauteuilVendu() * sT.getSalleTheatre().getNbFauteuil() ;
            }
            if(s instanceof SeanceFilm){
                SeanceFilm sF = (SeanceFilm) s;
                benefPlaceTN += sF.getNbPlaceVendueTN() * sF.getSalle().getTarifPlace() ;
                benefPlaceTR += sF.getNbPlaceVendueTR() * sF.getSalle().tarifReduit(sF.getSalle().getTarifPlace()) ;
            }
        }
        return benefFauteuil + benefPlaceTN + benefPlaceTR ;
    }

    public boolean equals(Object o){
        if(this == o) return true ;
        if(o == null || getClass() != o.getClass()) return false ;
        Spectacle that = (Spectacle) o ;
        return this.getListeSeance().equals(that.getListeSeance()) ;
    }
}
